package com.example.fidoreqdemoapp

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import com.example.fidoreqdemoapp.ui.theme.FIDOReqDemoAppTheme
import kotlin.random.Random

const val rpId = "fido.demo.example"

class MainActivity : ComponentActivity() {

    private var createHMACResultLauncher: ActivityResultLauncher<Intent>? = null
    private var getHMACResultLauncher: ActivityResultLauncher<Intent>? = null
    private var credential: ByteArray? = null
    private var status = mutableStateOf("")

    fun doCreate() {
        createHMACResultLauncher?.launch(
            Intent("android.fido.intent.action.HMAC_SECRET_CREATE").apply {
                putExtra("rpId", rpId)
            }
        )
    }

    fun doGet() {
        if (credential == null) {
            return
        }

        getHMACResultLauncher?.launch(
            Intent("android.fido.intent.action.HMAC_SECRET_CHALLENGE_RESPONSE").apply {
                putExtra("rpId", rpId)
                putExtra("numCredentials", 1)
                putExtra("credential_0", credential)
                putExtra("challenge", Random.Default.nextBytes(32))
            }
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        val createResultCallback = ActivityResultCallback<ActivityResult> { result ->
            credential = if (result.resultCode == Activity.RESULT_OK) {
                status.value = "Creation successful"
                result.data?.getByteArrayExtra("credentialId")
            } else {
                status.value = "Create failed"
                null
            }
        }

        val getResultCallback = ActivityResultCallback<ActivityResult> { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val gottenCred = result.data?.getByteArrayExtra("credentialId")
                if (gottenCred.contentEquals(credential)) {
                    status.value = "Get successful"
                } else {
                    status.value = "Mismatched cred ID"
                }
            } else {
                status.value = "Get failed"
            }
        }

        createHMACResultLauncher = this.registerForActivityResult(
            ActivityResultContracts.StartActivityForResult(),
            createResultCallback
        )
        getHMACResultLauncher = this.registerForActivityResult(
            ActivityResultContracts.StartActivityForResult(),
            getResultCallback
        )

        setContent {
            FIDOReqDemoAppTheme {
                val textToShow by status
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
                    Column {
                        Button(onClick = ::doCreate) {
                            Text("Create")
                        }
                        Button(onClick = ::doGet) {
                            Text("Get")
                        }
                        Text(text = textToShow)
                    }
                }
            }
        }
    }
}